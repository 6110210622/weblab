using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Pharmacy.Data;
using Pharmacy.Models;

namespace Pharmacy.Models
{
    public class  StaffUser: IdentityUser{
        public string FirstName { get; set;}
        public string LastName { get; set;}
     }

    public class TypeStock {
        public int TypeStockID { get; set;}
        public string TypeName {get; set;}
    }
    public class Stock {
        public int StockID { get; set; }

        public int TypeStockID { get; set;}
        public TypeStock TyStock { get; set; }

        [DataType(DataType.Date)]
        public string StockDate { get; set; }
        public string MedicineName { get; set; }
        public string MedicineDetail { get; set; }
        public int Amountdue { get; set; }

        public string StaffUserID {get; set;}
        public StaffUser PostUser {get; set;}
    }

}
