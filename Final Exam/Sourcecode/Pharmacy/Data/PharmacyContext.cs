using Pharmacy.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace Pharmacy.Data
{
    public class PharmacyContext : IdentityDbContext<StaffUser>
    {
        public DbSet<Stock> StockList { get; set; }
        public DbSet<TypeStock> StockDB { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        optionsBuilder.UseSqlite(@"Data source=Pharmacy.db");
        }
    }
}